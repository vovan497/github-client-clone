import React, { useState } from "react";
import Home from "./pages/Home";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import FilesTree from "./pages/FilesTree";
import Search from "./components/Search/Search";
import Container from "@mui/material/Container";
import RepositoryPage from "./pages/RepositoryPage";

const App: React.FC = () => {
  const [repositories, setRepositories] = useState<[]>([]);

  const editRepositories = (repos: []) => setRepositories(repos);

  return (
    <Container maxWidth="lg">
      <Router>
        <Search editRepositories={editRepositories} />
        <Routes>
          <Route path="/" element={<Home repositories={repositories} />} />
          <Route
            path="/:repositoryOwner/:repositoryName"
            element={<RepositoryPage />}
          />
          <Route
            path="/:repositoryOwner/:repositoryName/tree/:branchName/:treeId"
            element={<FilesTree />}
          />
        </Routes>
      </Router>
    </Container>
  );
};

export default App;
