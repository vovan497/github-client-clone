import List from "@mui/material/List";
import RepositoryListItem from "../RepositoryListItem/RepositoryListItem";

interface RepositoriesListProps {
  repositories: [];
}

interface Repository {
  id: string;
  nameWithOwner: string;
  url: string;
  description: string;
  labels: {
    edges: [
      {
        node: {
          id: string;
          name: string;
          url: string;
        };
      }
    ];
  };
  stargazerCount: number;
  languages: {
    edges: [
      {
        node: {
          id: string;
          name: string;
          color: string;
        };
      }
    ];
  };
  licenseInfo: {
    name: string;
  };
  updatedAt: string;
}

const RepositoriesList: React.FC<RepositoriesListProps> = ({
  repositories,
}) => (
  <List>
    {repositories.map((repository: Repository) => (
      <RepositoryListItem key={repository.id} repository={repository} />
    ))}
  </List>
);

export default RepositoriesList;
