import { Link } from "react-router-dom";
import Chip from "@mui/material/Chip";
import { Link as MUILink } from "@mui/material";
import Stack from "@mui/material/Stack";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import StarBorderIcon from "@mui/icons-material/StarBorder";

interface Repository {
  id: string;
  nameWithOwner: string;
  url: string;
  description: string;
  labels: {
    edges: [
      {
        node: {
          id: string;
          name: string;
          url: string;
        };
      }
    ];
  };
  stargazerCount: number;
  languages: {
    edges: [
      {
        node: {
          id: string;
          name: string;
          color: string;
        };
      }
    ];
  };
  licenseInfo: {
    name: string;
  };
  updatedAt: string;
}

interface RepositoryListItemProps {
  repository: Repository;
}

const RepositoryListItem: React.FC<RepositoryListItemProps> = ({
  repository,
}) => (
  <ListItem
    divider={true}
    disableGutters={true}
    alignItems="flex-start"
    sx={{ padding: "24px 0" }}
  >
    <Stack direction="column">
      <Link to={repository.nameWithOwner}>
        <MUILink>{repository.nameWithOwner}</MUILink>
      </Link>
      <ListItemText primary={repository.description} />
      <Stack direction="row" spacing={1}>
        {repository.labels.edges.map((label) => (
          <Chip
            clickable
            size="small"
            component="a"
            key={label.node.id}
            href={label.node.url}
            label={label.node.name}
          />
        ))}
      </Stack>
      <Stack
        direction="row"
        spacing={2}
        sx={{ alignItems: "center", fontSize: "14px" }}
      >
        <Link to={`${repository.url}/stargazers`}>
          <Stack direction="row" sx={{ alignItems: "center" }}>
            <StarBorderIcon fontSize="small" />
            {repository.stargazerCount}
          </Stack>
        </Link>
        <Stack direction="row" sx={{ alignItems: "center" }}>
          <span
            style={{
              width: "12px",
              height: "12px",
              borderRadius: "50%",
              backgroundColor: `${repository.languages.edges[0].node.color}`,
            }}
          ></span>
          <ListItemText sx={{ marginLeft: "4px" }}>
            <span style={{ fontSize: "14px" }}>
              {repository.languages.edges[0].node.name}
            </span>
          </ListItemText>
        </Stack>
        <span>{repository.licenseInfo.name}</span>
        <span>{repository.updatedAt}</span>
      </Stack>
    </Stack>
  </ListItem>
);

export default RepositoryListItem;
