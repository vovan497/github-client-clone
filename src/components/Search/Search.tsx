import React, { useState } from "react";
import { loader } from "graphql.macro";
import { useLazyQuery } from "@apollo/client";
import { useNavigate, useLocation } from "react-router-dom";
import TextField from "@mui/material/TextField";

const FIND_REPOSITORIES_QUERY = loader(
  "../../queries/FindRepositories.graphql"
);

interface SearchProps {
  editRepositories: (a: []) => void;
}

const Search: React.FC<SearchProps> = ({ editRepositories }) => {
  const [search, setSearch] = useState("");
  const navigate = useNavigate();
  const location = useLocation();

  const [findRepositories] = useLazyQuery(FIND_REPOSITORIES_QUERY, {
    variables: {
      searchString: search,
      elementsQuantity: 1,
    },
    onCompleted: ({ search: { nodes } }) => editRepositories(nodes),
  });

  const handleKeyPress = (e: React.KeyboardEvent) => {
    const target = e.target as typeof e.target & { value: string };
    setSearch(target.value.trim());

    if (e.code === "Enter") {
      findRepositories();

      if (location.pathname !== "/") navigate("/");
    }
  };

  return (
    <>
      <TextField
        type="search"
        variant="standard"
        id="standard-search"
        label="Search field"
        sx={{ margin: "16px 0 32px" }}
        onKeyPress={(e) => handleKeyPress(e)}
      />
    </>
  );
};

export default Search;
