import { loader } from "graphql.macro";
import { useParams, Link } from "react-router-dom";
import { useQuery } from "@apollo/client";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import FolderIcon from "@mui/icons-material/Folder";
import InsertDriveFileIcon from "@mui/icons-material/InsertDriveFile";

const REPOSITORY_FILES_TREE = loader("../queries/RepositoryFilesTree.graphql");

interface RepositoryFile {
  name: string;
  type: string;
  oid: string;
}

const FilesTree: React.FC = () => {
  const { repositoryOwner, repositoryName, treeId } = useParams();

  const { data } = useQuery(REPOSITORY_FILES_TREE, {
    variables: {
      repositoryName,
      repositoryOwner,
      fileTreeId: treeId,
    },
  });

  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell></TableCell>
            <TableCell></TableCell>
            <TableCell></TableCell>
            <TableCell></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data ? (
            data.repository.object.entries.map((file: RepositoryFile) => (
              <TableRow
                key={file.name}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="td" scope="row">
                  {file.type === "tree" ? (
                    <FolderIcon />
                  ) : (
                    <InsertDriveFileIcon />
                  )}
                </TableCell>
                <TableCell component="td" scope="row">
                  <Link to={file.oid}>{file.name} </Link>
                </TableCell>
                <TableCell component="td" scope="row"></TableCell>
                <TableCell component="td" scope="row"></TableCell>
              </TableRow>
            ))
          ) : (
            <TableRow />
          )}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default FilesTree;
