import RepositoriesList from "../components/RepositoriesList/RepositoriesList";

interface HomeProps {
  repositories: [];
}

const Home: React.FC<HomeProps> = ({ repositories }) => (
  <>
    <RepositoriesList repositories={repositories} />
  </>
);

export default Home;
