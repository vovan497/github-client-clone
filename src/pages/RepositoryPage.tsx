import { useEffect, useState } from "react";
import { client } from "../index";
import { loader } from "graphql.macro";
import { useQuery } from "@apollo/client";
import { useParams, Link } from "react-router-dom";
import ReactMarkdown from "react-markdown";
import Card from "@mui/material/Card";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import FolderIcon from "@mui/icons-material/Folder";
import InsertDriveFileIcon from "@mui/icons-material/InsertDriveFile";
import { treeEntryQuery } from "../queries/GetTreeEntryData";
import sortByType from "../utils/sortByType";

const REPOSITORY_MAIN_PAIGE = loader("../queries/RepositoryMainPaige.graphql");
const GET_README = loader("../queries/GetReadme.graphql");

interface RepositoryFile {
  name: string;
  mode: number;
  type: string;
  path: string;
  oid: string;
  lastCommitMessage: string;
  lastCommitDate: string;
}

const RepositoryPage: React.FC = () => {
  const [files, setFiles] = useState<RepositoryFile[]>([]);
  const { repositoryOwner, repositoryName } = useParams();

  const { data } = useQuery(REPOSITORY_MAIN_PAIGE, {
    variables: {
      repositoryName,
      repositoryOwner,
    },
  });

  useEffect(() => {
    if (data) {
      client
        .query({
          query: treeEntryQuery(
            data.repository.object.entries,
            repositoryOwner,
            repositoryName
          ),
        })
        .then((response) => {
          let entries: RepositoryFile[] = [];
          data.repository.object.entries.forEach(
            (entry: RepositoryFile, i: number) => {
              const entryObject =
                response.data.repository.object[entry.name.replace(".", "")]
                  .edges[0].node;

              entries = [
                ...entries,
                {
                  ...entry,
                  lastCommitMessage: entryObject.message,
                  lastCommitDate: entryObject.pushedDate,
                },
              ];
            }
          );

          setFiles(sortByType(entries));
        });
    }
  }, [data, repositoryOwner, repositoryName]);

  const readmeFile = useQuery(GET_README, {
    variables: {
      repositoryName,
      repositoryOwner,
    },
  });

  return (
    <>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} size="small" aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell></TableCell>
              <TableCell></TableCell>
              <TableCell></TableCell>
              <TableCell></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {files.map((file: RepositoryFile) => (
              <TableRow
                key={file.name}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="td" scope="row">
                  {file.type === "tree" ? (
                    <FolderIcon />
                  ) : (
                    <InsertDriveFileIcon />
                  )}
                </TableCell>
                <TableCell component="td" scope="row">
                  <Link
                    to={`/${repositoryOwner}/${repositoryName}/tree/${data.repository.defaultBranchRef.name}/${file.oid}`}
                  >
                    {file.name}
                  </Link>
                </TableCell>
                <TableCell component="td" scope="row">
                  {file.lastCommitMessage}
                </TableCell>
                <TableCell component="td" scope="row">
                  {file.lastCommitDate}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Card
        variant="outlined"
        sx={{
          boxShadow: 1,
          marginTop: "16px",
          padding: "16px 32px 32px",
        }}
      >
        <ReactMarkdown
          children={
            readmeFile.data ? readmeFile.data.repository.object.text : ""
          }
        />
      </Card>
    </>
  );
};

export default RepositoryPage;
