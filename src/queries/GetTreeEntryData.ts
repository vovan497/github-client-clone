import { gql } from "@apollo/client";

interface File {
  name: string;
  mode: number;
  type: string;
  path: string;
  oid: string;
}

const treeEntryQuery = (
  files: [File],
  repositoryOwner?: string,
  repositoryName?: string
) => {
  const query = (aliases: string) =>
    `query {
      repository(name: "${repositoryName}", owner: "${repositoryOwner}") {
        object(expression: "HEAD") {
          ... on Commit {
            id
            ${aliases}
          }
        }
      }
    }`;

  const historyAlias = (key: string, path: string) =>
    `
    ${key}: history(first: 1, path: "${path}") {
      edges {
        node {
          id
          message
          pushedDate
        }
      }
    }`;

  let historyAliases = ``;

  for (let i = 0; i < files.length; i++) {
    const name = files[i].name.replace(".", "");
    historyAliases = `${historyAliases ? historyAliases : ``} ${historyAlias(
      name,
      files[i].name
    )}`;
  }

  return gql`
    ${query(historyAliases)}
  `;
};

export { treeEntryQuery };
