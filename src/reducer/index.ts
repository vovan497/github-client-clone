const initialState = {
  treeId: "",
};

interface State {
  treeId: string;
}

interface Action {
  type: string;
  payload: string;
}

function reducer(state: State, action: Action) {
  switch (action.type) {
    case "SET_TREE_ID":
      return {
        ...state,
        treeId: action.payload,
      };
    default:
      throw new Error('Action should have "type" property');
  }
}

export { reducer, initialState };
