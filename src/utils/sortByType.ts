interface RepositoryFile {
  name: string;
  mode: number;
  type: string;
  path: string;
  oid: string;
  lastCommitMessage: string;
  lastCommitDate: string;
}

const sortByType = (files: RepositoryFile[]) => {
  let directories: RepositoryFile[] = [];
  let blobs: RepositoryFile[] = [];
  let sortedFiles: RepositoryFile[] = [];

  files.forEach((file: RepositoryFile) =>
    file.type === "tree"
      ? (directories = [...directories, file])
      : (blobs = [...blobs, file])
  );
  sortedFiles = [...directories, ...blobs];

  return sortedFiles;
};

export default sortByType;
